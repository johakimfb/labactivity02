package hellopackage;

import java.util.Scanner;
import java.util.Random;
import secondpackage.Utilities;

class Greeter {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        java.util.Random rand = new java.util.Random();

        System.out.println("Input an integer: ");
        int number = input.nextInt();

        System.out.println(Utilities.doubleMe(number));
    }  
}